# Entx

## O contexto da proposta é o desenvolvimento de uma plataforma para aluguel de carros de luxo. No momento, é possível fazer o cadastro do cliente além das opções de excluir ou editar dados e pesquisar por um nome na tabela.

##
##

### Executando o projeto:
##
``Yarn install`` para instalar as dependências
##
``Yarn dev:server`` para executar api fake
##
``Yarn start`` para executar o projeto
##
``Yarn test`` para executar os testes (tem um exemplo pra cada 'divisão' do projeto)
##
##

### Para acessar o sistema é preciso criar uma conta ou entrar com email ``admin@hotmail.com`` e senha ``12345``

### Demonstração:
#
![Des](.bitbucket/1.gif)
##
#### Testando email em formato inválido
![Des](.bitbucket/2.gif)
##
#### Usando cep aleatório de São Paulo
![Des](.bitbucket/3.gif)
