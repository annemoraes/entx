import React from 'react';
import { Switch } from 'react-router-dom';

import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import Landing from '../pages/Landing';
import Register from '../pages/Register';
import User from '../pages/User';

import Route from './Route';

const Routes: React.FC = () => {
  return (
    <Switch>
      <Route path="/" exact component={SignIn} />
      <Route path="/signup" component={SignUp} />
      <Route path="/landing" component={Landing} isPrivate />
      <Route path="/register" component={Register} isPrivate />
      <Route path="/user" component={User} isPrivate />
    </Switch>
  );
};

export default Routes;
