import styled, { css } from 'styled-components';
import Tooltip from '../Tooltip';

interface ContainerProps {
  isFocused: boolean;
  isFilled: boolean;
  isErrored: boolean;
}

export const Container = styled.div<ContainerProps>`
  background: #fff;
  border-radius: 6px;
  padding: 14px;
  width: 100%;
  height: 40px;

  display: flex;
  align-items: center;
  flex-direction: row !important;

  color: #666360;
  border: 1px solid #dcdcdc;

  ${props =>
    props.isErrored &&
    css`
      border-color: #dc1637;
    `}

  ${props =>
    props.isFocused &&
    css`
      color: #000080;
      border-color: #000080;
    `}

  ${props =>
    props.isFilled &&
    css`
      color: #000080;
    `}

  & + div {
    margin-top: 8px;
  }

  input {
    flex: 1;
    background: transparent;
    border: 0;
    color: #1d1b23;
    width: 100% !important;

    &::placeholder {
      color: #666360;
    }
  }

  svg {
    margin-right: 16px;
  }
`;

export const Error = styled(Tooltip)`
  height: 20px;
  width: 20px !important;
  //align-self: flex-end;
  margin-left: 16px;
  svg {
    margin: 0;
  }
  span {
    background: #c53030;
    color: #fff;
    &::before {
      border-color: #c53030 transparent;
    }
  }
`;
