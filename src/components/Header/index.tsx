import React from 'react';
import { Link } from 'react-router-dom';
import { FiPower } from 'react-icons/fi';
import { useAuth } from '../../hooks/auth';

import { Container } from './styles';

import Logo from '../../assets/logo.svg';

const Header: React.FC = () => {
  const { signOut } = useAuth();

  return (
    <Container>
      <header>
        <img src={Logo} alt="app" />
        <nav>
          <>
            <Link to="/landing">
              <div data-testid="header-testid" />
              Listagem
            </Link>
            <Link to="/register">Adicionar Usuário</Link>
            <button type="button">
              <FiPower size={20} onClick={signOut} />
            </button>
          </>
        </nav>
      </header>
    </Container>
  );
};

export default Header;
