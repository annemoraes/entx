import styled from 'styled-components';

import { shade } from 'polished';
import signInBackground from '../../assets/sign-in-background.png';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  align-items: stretch;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  place-content: center;
  width: 100%;
  max-width: 610px;
  background: #3a3547;
`;

export const FormContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  width: 400px;
  height: 500px;
  border-radius: 1px;
  background: #fff;

  form {
    margin: 30px 0;
    width: 340px;
    text-align: center;
    h1 {
      margin-bottom: 40px;
    }

    a {
      color: #1d1b23;
      font-size: 14px;
      display: block;
      margin-top: 24px;
      text-decoration: none;
      transition: color 0.2s;
    }
  }

  > a {
    color: #000080;
    display: block;
    text-decoration: none;
    transition: color 0.2s;
    display: flex;
    align-items: center;
    svg {
      margin-right: 14px;
    }
    &:hover {
      color: ${shade(0.2, '#000080')};
    }
  }
`;

export const Background = styled.div`
  flex: 1;
  background: url(${signInBackground}) no-repeat center;
  background-size: cover;
`;
