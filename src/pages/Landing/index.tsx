import React, { useState, useEffect, useCallback, useRef } from 'react';
import { useLocation, useHistory } from 'react-router-dom';

import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import InfiniteLoading from 'react-simple-infinite-loading';
import * as S from './styles';
import Header from '../../components/Header';
import api from '../../services/api';
import { useToast } from '../../hooks/toast';
import UserDTO from '../../hooks/userDTO';

import Input from '../../components/Input';
import Edit from '../../assets/pencil.png';
import Delete from '../../assets/delete.png';

interface LoadUser {
  nome: string;
}

const Landing: React.FC = () => {
  const [users, setUsers] = useState<UserDTO[]>([]);
  const formRef = useRef<FormHandles>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [count, setCount] = useState<number>(1);

  const { addToast } = useToast();
  const history = useHistory();

  useEffect(() => {
    const loadUsers = async (): Promise<void> => {
      try {
        setLoading(true);
        const response = await api.get('/usuarios?_page=1&_limit=4');

        setUsers(response.data);
        setLoading(false);
      } catch (err) {
        setLoading(false);

        console.log(err);
      }
    };
    loadUsers();
  }, []);

  const handleSearch = useCallback(async (data: LoadUser): Promise<void> => {
    try {
      const response = await api.get(
        `/usuarios${data.nome.length === 0 ? '/' : `/?nome=${data.nome}`}`,
      );

      setUsers(response.data);
    } catch (err) {
      addToast({
        type: 'error',
        title: 'Nenhum usuário encontrado com esse nome',
        description: err.message,
      });
    }
  }, []);

  const listUser = async () => {
    setLoading(true);
    const response = await api.get(`/usuarios?_page=${count}&_limit=5`);

    const moreUsers = response.data;

    setCount(count + 1);

    setUsers([...users, ...moreUsers]);
    setLoading(false);
  };

  const handleUserRemove = useCallback(
    async (id: string | undefined) => {
      await api.delete(`/usuarios/${id}`);
      setUsers(state => state.filter(user => user.id !== id));
      addToast({
        type: 'success',
        title: 'Remoção realizada com sucesso',
        description: '',
      });
    },
    [addToast],
  );

  return (
    <>
      <Header />
      <S.Container>
        <S.Content>
          <S.Table>
            <Form ref={formRef} onSubmit={handleSearch}>
              <S.InputContainer>
                <Input name="nome" type="text" placeholder="Pesquisar nome" />
              </S.InputContainer>
            </Form>
            <ul>
              {loading}
              <h1>Usuários</h1>
              <S.Title>
                <span>Nome</span>
                <span>Email</span>
                <span>CPF</span>
                <span>Cidade</span>
              </S.Title>
              <div style={{ height: '57vh' }}>
                <InfiniteLoading
                  hasMoreItems
                  itemHeight={90}
                  loadMoreItems={listUser}
                >
                  {users.map(user_ => (
                    <li key={user_.id}>
                      <div>
                        <p>{user_.nome}</p>
                      </div>
                      <div>
                        <p>{user_.email}</p>
                      </div>
                      <div>
                        <p>{user_.cpf}</p>
                      </div>
                      <div>
                        <p>{user_.endereco.cidade}</p>
                      </div>
                      <S.IconsContainer>
                        <S.CustomButton
                          disabled={!!loading}
                          onClick={() => handleUserRemove(user_.id)}
                        >
                          <img src={Delete} alt="" />
                        </S.CustomButton>
                        <S.CustomButton>
                          <img src={Edit} alt="" />
                        </S.CustomButton>
                      </S.IconsContainer>
                    </li>
                  ))}
                </InfiniteLoading>
              </div>
            </ul>
          </S.Table>
        </S.Content>
      </S.Container>
    </>
  );
};

export default Landing;
