import styled from 'styled-components';
import { lighten, shade } from 'polished';

export const Container = styled.div`
  min-height: 100vh;
  max-height: 100%;
  flex: 1;
`;

export const Content = styled.div`
  display: flex;
  padding: 90px 70px 50px 50px;
`;

export const Table = styled.div`
  flex: 1;
  ul {
    flex: 1;
    width: 100%;
    height: 100%;
    position: relative;
    background: #fff;
    border-radius: 4px;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.25);
    margin: 60px;
    padding: 20px 20px 0 20px;
    h1 {
      color: #1d1b23;
      margin: 0 0 20px 20px;
      font-size: 20px;
      font-family: 'Roboto Slab', serif;
    }
    li {
      display: flex;
      background: #fff;
      border-radius: 4px;
      padding: 20px;
      align-items: center;
      position: relative;
      border: 1px solid rgba(0, 0, 0, 0.25);
      box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.25);
      z-index: 0;
      div {
        flex: 1;
        margin-left: 20px;
      }

      p {
        color: #8497ae;
        font-size: 18px;
        margin-left: 5px;
        margin-top: 7px;
      }
      & + li {
        margin-top: 15px;
      }
    }
  }
`;

export const Title = styled.div`
  display: flex;
  width: 100%;
  span {
    flex: 1;
    margin-left: 35px;
    margin-bottom: 10px;
  }
`;

export const IconsContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  right: 0;
  height: 100%;
  width: 50px;
`;

export const CustomButton = styled.button`
  background-color: #f08080;
  color: ${shade(0.5, '#c53030')};
  border: none;
  position: relative;
  border-radius: 0 2px 0 0;
  height: 50%;
  transition: background-color 0.25s;
  &:hover {
    background-color: #c53030;
    color: ${shade(0.6, '#c53030')};
  }
  & + button {
    background-color: #b0e0e6;
    color: ${shade(0.5, '#48D1CC')};
    border-radius: 0 0 2px 0;
    transition: background-color 0.25s;
    &:hover {
      background-color: #48d1cc;
      color: ${shade(0.5, '#48D1CC')};
    }
  }
`;

export const InputContainer = styled.div`
  position: fixed;
  right: 50px;
  top: 115px;
  margin-bottom: 10px;
  width: 300px !important;
  z-index: 1;
`;
