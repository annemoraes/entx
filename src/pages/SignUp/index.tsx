import React, { useState, useCallback, useRef } from 'react';

import { FormHandles, Scope } from '@unform/core';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';

import {
  Container,
  FormContent,
  FormSection,
  ButtonRegister,
  Header,
} from './styles';
import { useCreate } from '../../hooks/user';
import { useToast } from '../../hooks/toast';

import UserDTO from '../../hooks/userDTO';

import getValidationErrors from '../../utils/getValidationErrors';
import Input from '../../components/Input';
import Logo from '../../assets/logo.svg';

import cpfMask from '../../utils/cpfMask';
import cepMask from '../../utils/cepMask';
import apiCep from '../../services/apiCep';

const SignUp: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const [cpfMasked, setCpfMasked] = useState('');
  const [cepMasked, setCepMasked] = useState('');
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const { addToast } = useToast();
  const { createUser } = useCreate();

  const handleSubmit = useCallback(
    async (data: UserDTO): Promise<void> => {
      setLoading(true);
      try {
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          nome: Yup.string().required('Nome é obrigatório'),
          cpf: Yup.string().min(14, 'Precisa ter 12 números'),
          senha: Yup.string().min(5, 'No mínimo 5 dígitos'),
          email: Yup.string()
            .email('Digite um email válido')
            .required('O email é obrigatório'),
          endereco: Yup.object().shape({
            cep: Yup.string().min(9, 'Precisa ter 8 números'),
            bairro: Yup.string().required('Bairro é obrigatório'),
            rua: Yup.string().required('Rua é obrigatória'),
            numero: Yup.string().required('Número é obrigatório'),
            cidade: Yup.string().required('Cidade é obrigatória'),
          }),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        await createUser(data);

        addToast({
          type: 'success',
          title: 'Cadastro realizado, faça seu login',
          description: `Olá, ${data.nome}!`,
        });

        history.push('/landing');
        setLoading(false);
      } catch (err) {
        setLoading(false);
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);
          formRef.current?.setErrors(errors);
          return;
        }

        addToast({
          type: 'error',
          title: 'Erro no cadastro',
          description: err.message,
        });
      }
    },
    [createUser, addToast],
  );

  const handleMaskCep = useCallback(async (e: any): Promise<any> => {
    setCepMasked(cepMask(e.target.value));
    const cep = e.target.value;

    const response = await apiCep.get(`${cep.replace('-', '')}/json/`);

    formRef.current?.setFieldValue('endereco.cidade', response.data.localidade);
    formRef.current?.setFieldValue('endereco.rua', response.data.logradouro);
    formRef.current?.setFieldValue('endereco.bairro', response.data.bairro);

    if (response.data.logradouro) {
      setTimeout(() => {
        formRef.current?.getFieldRef('endereco.numero').focus();
      }, 500);
    }
  }, []);

  const handleMaskCpf = useCallback((e: any): any => {
    setCpfMasked(cpfMask(e.target.value));
  }, []);

  return (
    <>
      <Header>
        <img src={Logo} alt="" />
      </Header>
      <Container>
        <FormContent ref={formRef} onSubmit={handleSubmit}>
          <h2>Insira os dados</h2>
          <div>
            <FormSection>
              <Input name="nome" placeholder="Digite seu nome" />
              <Input
                name="cpf"
                placeholder="Digite seu CPF"
                value={cpfMasked}
                onChange={handleMaskCpf}
              />
              <Input name="email" placeholder="Digite seu email" />

              <Input
                name="senha"
                type="password"
                placeholder="Digite sua senha"
              />
              <Scope path="endereco">
                <Input
                  name="cep"
                  placeholder="CEP"
                  value={cepMasked}
                  onChange={handleMaskCep}
                />
                <Input name="rua" placeholder="Rua" />
                <Input name="bairro" placeholder="Bairro" />
                <Input name="numero" placeholder="Nº" />
                <Input name="cidade" placeholder="Cidade" />
              </Scope>
            </FormSection>
          </div>

          <ButtonRegister disabled={!!loading} loading={loading} type="submit">
            Cadastrar
          </ButtonRegister>
        </FormContent>
      </Container>
    </>
  );
};

export default SignUp;
