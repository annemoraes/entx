import React, { useState, useCallback, useRef, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';

import { FormHandles, Scope } from '@unform/core';
import * as Yup from 'yup';

import { Container, FormContent, FormSection, ButtonRegister } from './styles';
import { useCreate } from '../../hooks/user';
import { useToast } from '../../hooks/toast';
import { useAuth } from '../../hooks/auth';

import UserDTO from '../../hooks/userDTO';

import getValidationErrors from '../../utils/getValidationErrors';
import Input from '../../components/Input';
import Header from '../../components/Header';
import cpfMask from '../../utils/cpfMask';
import cepMask from '../../utils/cepMask';
import apiCep from '../../services/apiCep';

interface StateProps {
  state?: any;
}

const User: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const [cpfMasked, setCpfMasked] = useState('');
  const [cepMasked, setCepMasked] = useState('');
  const [loading, setLoading] = useState(false);

  const { addToast } = useToast();
  const { updateUser } = useCreate();

  const { signIn } = useAuth();
  const history = useHistory();
  const { state }: StateProps = useLocation();

  useEffect(() => {
    formRef.current?.setFieldValue('nome', state.user.nome);
    formRef.current?.setFieldValue('cpf', state.user.cpf);
    formRef.current?.setFieldValue('email', state.user.email);
    formRef.current?.setFieldValue('senha', state.user.password);
    formRef.current?.setFieldValue('endereco.cep', state.user.endereco.cep);
    formRef.current?.setFieldValue('endereco.rua', state.user.endereco.rua);
    formRef.current?.setFieldValue(
      'endereco.bairro',
      state.user.endereco.bairro,
    );
    formRef.current?.setFieldValue(
      'endereco.numero',
      state.user.endereco.numero,
    );
    formRef.current?.setFieldValue(
      'endereco.cidade',
      state.user.endereco.cidade,
    );
  }, []);

  const handleSubmit = useCallback(
    async (data: UserDTO): Promise<void> => {
      setLoading(true);
      try {
        formRef.current?.setErrors({});

        const schema = Yup.object().shape({
          nome: Yup.string().required('Nome é obrigatório'),
          cpf: Yup.string().min(14, 'Precisa ter 12 números'),
          senha: Yup.string().min(5, 'No mínimo 5 dígitos'),
          email: Yup.string()
            .email('Digite um email válido')
            .required('O email é obrigatório'),
          endereco: Yup.object().shape({
            cep: Yup.string().min(9, 'Precisa ter 8 números'),
            bairro: Yup.string().required('Bairro é obrigatório'),
            rua: Yup.string().required('Rua é obrigatória'),
            numero: Yup.string().required('Número é obrigatório'),
            cidade: Yup.string().required('Cidade é obrigatória'),
          }),
        });

        await schema.validate(data, {
          abortEarly: false,
        });

        const upUser: UserDTO = {
          id: state?.user?.id,
          nome: data.nome,
          cpf: data.cpf,
          email: data.email,
          senha: data.senha,
          endereco: {
            cep: data.endereco.cep,
            rua: data.endereco.rua,
            bairro: data.endereco.bairro,
            numero: data.endereco.numero,
            cidade: data.endereco.cidade,
          },
        };

        await updateUser(upUser);

        if (state?.id === 'user') {
          await signIn({ email: data.email, password: data.senha });
        }
        addToast({
          type: 'success',
          title: 'Cadastro atualizado',
          description: `Dados atualizados`,
        });

        history.push('/landing');
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);
          formRef.current?.setErrors(errors);
          return;
        }
        addToast({
          type: 'error',
          title: 'Erro na atualização',
          description: err.message,
        });
      }
    },
    [updateUser, addToast],
  );

  const handleMaskCep = useCallback(async (e: any): Promise<any> => {
    setCepMasked(cepMask(e.target.value));
    const cep = e.target.value;

    const response = await apiCep.get(`${cep.replace('-', '')}/json/`);

    formRef.current?.setFieldValue('endereco.cidade', response.data.localidade);
    formRef.current?.setFieldValue('endereco.rua', response.data.logradouro);
    formRef.current?.setFieldValue('endereco.bairro', response.data.bairro);

    if (response.data.logradouro) {
      setTimeout(() => {
        formRef.current?.getFieldRef('endereco.numero').focus();
      }, 500);
    }
  }, []);

  const handleMaskCpf = useCallback((e: any): any => {
    setCpfMasked(cpfMask(e.target.value));
  }, []);

  return (
    <>
      <Header />
      <Container>
        <FormContent ref={formRef} onSubmit={handleSubmit}>
          <h2>Insira os dados</h2>
          <div>
            <FormSection>
              <Input name="nome" placeholder="Digite seu nome" />
              <Input
                name="cpf"
                placeholder="Digite seu CPF"
                value={cpfMasked}
                onChange={handleMaskCpf}
              />
              <Input name="email" placeholder="Digite seu email" />

              <Input
                name="senha"
                type="password"
                placeholder="Digite sua senha"
              />
              <Scope path="endereco">
                <Input
                  name="cep"
                  placeholder="CEP"
                  value={cepMasked}
                  onChange={handleMaskCep}
                />
                <Input name="rua" placeholder="Rua" />
                <Input name="bairro" placeholder="Bairro" />
                <Input name="numero" placeholder="Nº" />
                <Input name="cidade" placeholder="Cidade" />
              </Scope>
            </FormSection>
          </div>

          <ButtonRegister disabled={!!loading} loading={loading} type="submit">
            Atualizar
          </ButtonRegister>
        </FormContent>
      </Container>
    </>
  );
};

export default User;
