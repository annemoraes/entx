import { renderHook, act } from '@testing-library/react-hooks';
import MockAdapter from 'axios-mock-adapter';

import { useAuth, AuthProvider } from '../../hooks/auth';
import api from '../../services/api';

const apiMock = new MockAdapter(api);

const mockedHistoryPush = jest.fn();
const mockedAddToast = jest.fn();

const apiResponse = {
  id: '123',
  nome: 'Anne',
  email: 'anne@example.com',
  password: '12345',
  token: 'token-123',
};

jest.mock('react-router-dom', () => {
  return {
    useHistory: () => ({
      push: mockedHistoryPush,
    }),
    Link: ({ children }: { children: React.ReactNode }) => children,
  };
});

jest.mock('../../hooks/toast', () => {
  return {
    useToast: () => ({
      addToast: mockedAddToast,
    }),
  };
});

describe('Auth hook', () => {
  beforeEach(() => {
    mockedHistoryPush.mockClear();
    mockedAddToast.mockClear();
  });

  it('should be able to sign in', async () => {
    apiMock.onGet('/usuarios').reply(200, [apiResponse]);

    const setLocalStorageSpy = jest.spyOn(Storage.prototype, 'setItem');

    const { result, waitForNextUpdate } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    result.current.signIn({ email: 'anne@example.com', password: '12345' });

    await waitForNextUpdate();

    expect(setLocalStorageSpy).toHaveBeenCalledWith(
      '@App:token',
      apiResponse.token,
    );
    expect(setLocalStorageSpy).toHaveBeenCalledWith(
      '@App:user',
      JSON.stringify(apiResponse),
    );
    expect(result.current.user.email).toEqual('anne@example.com');
  });

  it('should be able restore saved data from storage when auth inits', () => {
    jest.spyOn(Storage.prototype, 'getItem').mockImplementation(key => {
      switch (key) {
        case '@App:token':
          return apiResponse.token;
        case '@App:user':
          return JSON.stringify(apiResponse);
        default:
          return null;
      }
    });

    const { result } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    expect(result.current.user.email).toEqual('anne@example.com');
  });

  it('should be able to sign out', async () => {
    jest.spyOn(Storage.prototype, 'getItem').mockImplementation(key => {
      switch (key) {
        case '@App:token':
          return apiResponse.token;
        case '@App:user':
          return JSON.stringify(apiResponse);
        default:
          return null;
      }
    });

    const removeItemSpy = jest.spyOn(Storage.prototype, 'removeItem');

    const { result } = renderHook(() => useAuth(), {
      wrapper: AuthProvider,
    });

    act(() => result.current.signOut());

    expect(removeItemSpy).toHaveBeenCalledTimes(2);
    expect(result.current.user).toBeUndefined;
  });
});
