import React, { createContext, useCallback, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';

import api from '../services/api';

interface UserAddress {
  cep: string;
  street: string;
  number: number;
  neighborhood: string;
  city: string;
}

export interface CreateUsersDTO {
  id?: string;
  token?: string;
  name: string;
  cpf: string;
  email: string;
  password: string;
  address: UserAddress;
}

interface SignInCredentials {
  email: string;
  password: string;
}

interface AuthContextDTO {
  token: string;
  user: CreateUsersDTO;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
}

const AuthContext = createContext<AuthContextDTO>({} as AuthContextDTO);

export const AuthProvider: React.FC = ({ children }) => {
  const history = useHistory();

  const [token, setToken] = useState(() => {
    const newToken = localStorage.getItem('@App:token');

    if (newToken) {
      return newToken;
    }

    return '';
  });

  const [data, setData] = useState<CreateUsersDTO>(() => {
    const user = localStorage.getItem('@App:user');

    if (user) {
      return JSON.parse(user);
    }
    return {} as CreateUsersDTO;
  });

  const signIn = useCallback(async ({ email, password }) => {
    const response = await api.get('/usuarios');
    const users = response.data;
    const userExist = users.find((user: any) => user.email === email);

    if (!userExist) {
      throw new Error('Usuário não encontrado.');
    }

    if (userExist.password !== password) {
      throw new Error('E-mail/senha incorretos.');
    }

    if (userExist && userExist.password === password) {
      localStorage.setItem('@App:user', JSON.stringify(userExist));
      localStorage.setItem('@App:token', userExist.token);

      setToken(userExist.token);
      setData(userExist);
    }
  }, []);

  const signOut = useCallback(() => {
    localStorage.removeItem('@App:token');
    localStorage.removeItem('@App:user');

    setData({} as CreateUsersDTO);
    setToken('');
    history.push('/');
  }, [history]);

  return (
    <AuthContext.Provider value={{ user: data, signIn, signOut, token }}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth(): AuthContextDTO {
  const context = useContext(AuthContext);

  return context;
}
