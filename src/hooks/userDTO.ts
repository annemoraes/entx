interface UserAddress {
  cep: string;
  rua: string;
  numero: number;
  bairro: string;
  cidade: string;
}

export default interface UserDTO {
  id?: string;
  token?: string;
  nome: string;
  cpf: string;
  email: string;
  senha: string;
  endereco: UserAddress;
}
