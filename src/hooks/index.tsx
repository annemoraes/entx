import React from 'react';

import { CreateUserProvider } from './user';
import { AuthProvider } from './auth';
import { ToastProvider } from './toast';

const AppProvider: React.FC = ({ children }) => (
  <CreateUserProvider>
    <AuthProvider>
      <ToastProvider>{children}</ToastProvider>
    </AuthProvider>
  </CreateUserProvider>
);

export default AppProvider;
