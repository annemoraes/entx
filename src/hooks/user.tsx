import React, { createContext, useCallback, useState, useContext } from 'react';
import { uuid } from 'uuidv4';

import UserDTO from './userDTO';
import api from '../services/api';

interface CreateUserContextDTO {
  user: UserDTO;
  newUser: UserDTO;
  updateUser(credentials: UserDTO): Promise<void>;
  createUser(credentials: UserDTO): Promise<void>;
}

const CreateUserContext = createContext<CreateUserContextDTO>(
  {} as CreateUserContextDTO,
);

export const CreateUserProvider: React.FC = ({ children }) => {
  const [newData, setNewData] = useState<UserDTO>({} as UserDTO);

  const [data, setData] = useState<UserDTO>({} as UserDTO);

  const createUser = useCallback(async (credentials: UserDTO) => {
    const response = await api.get('/usuarios');

    const users = response.data;

    const userAlreadyExist = users.find(
      (user: any) => user.email === credentials.email,
    );

    if (userAlreadyExist) {
      throw new Error('Já existe um cadastro com esse e-mail.');
    }

    const form = {
      nome: credentials.nome,
      cpf: credentials.cpf,
      email: credentials.email,
      password: credentials.senha,
      token: uuid(),
      endereco: {
        cep: credentials.endereco.cep,
        rua: credentials.endereco.rua,
        numero: credentials.endereco.numero,
        bairro: credentials.endereco.bairro,
        cidade: credentials.endereco.cidade,
      },
    };

    const res = await api.post(
      '/usuarios',

      form,
    );

    setData(res.data);
  }, []);

  const updateUser = useCallback(async (credentials: UserDTO) => {
    const form = {
      nome: credentials.nome,
      cpf: credentials.cpf,
      email: credentials.email,
      password: credentials.senha,
      token: uuid(),
      endereco: {
        cep: credentials.endereco.cep,
        rua: credentials.endereco.rua,
        numero: credentials.endereco.numero,
        bairro: credentials.endereco.bairro,
        cidade: credentials.endereco.cidade,
      },
    };

    const response = await api.put(`/usuarios/${credentials.id}`, form);

    setData(response.data);
  }, []);

  return (
    <CreateUserContext.Provider
      value={{ user: data, createUser, updateUser, newUser: newData }}
    >
      {children}
    </CreateUserContext.Provider>
  );
};

export function useCreate(): CreateUserContextDTO {
  const context = useContext(CreateUserContext);

  return context;
}
